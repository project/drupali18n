<?php
/**
 * @file
 * Multilingual Drupal installation profile.
 */

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile,
 *   and optional 'language' to override the language selection for
 *   language-specific profiles, e.g., 'language' => 'fr'.
 */
function drupali18n_profile_details() {
  return array(
    'name' => 'Multilingual Drupal',
    'description' => 'Install Drupal with multilingual functionality provided by Internationalization and other contributed modules.',
  );
}

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * The following required core modules are always enabled:
 * 'block', 'filter', 'node', 'system', 'user'.
 *
 * @return
 *  An array of modules to be enabled.
 */
function drupali18n_profile_modules() {
  return array(
    // Core Drupal modules:
    'menu',
    'taxonomy',
    'locale',
    'translation',
    // Internationalization modules
    'i18n',
    'i18nstrings',
    'i18nblocks',
    'i18nmenu',
    'i18ntaxonomy',
    // Othercontrib modules
    'languageicons',
    'translation_overview'
  );
}

